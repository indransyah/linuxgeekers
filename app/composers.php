<?php

View::composer(['frontend.layouts.header', 'frontend.layouts.footer'], function($view)
{
  $sidebar1 = Sidebar::find(1);
  $sidebar2 = Sidebar::find(2);
  $sidebar3 = Sidebar::find(3);
  $sidebar4 = Sidebar::find(4);
  $company = Company::find(1);
  $view->with(array('sidebar1' => $sidebar1, 'sidebar2' => $sidebar2, 'sidebar3' => $sidebar3, 'sidebar4' => $sidebar4, 'company' => $company ));
});

View::composer(['frontend.layouts.master'], function($view) {
  $company = Company::find(1);
  $view->with(['company' => $company]);
});
