<?php

//use Category;
//use Validator;
//use Redirect;

class CategoryRepository implements CategoryInterface {

  protected $rules = [
    'name' => 'required|unique:categories,name'
  ];

  protected $validationErrors;

  public function all()
  {
    return Category::all();
  }

  public function find($id)
  {
    return Category::find($id);
  }

  public function store()
  {
    $category = new Category;
    $category->name = Input::get('name');
    $category->desc = Input::get('desc');
    if($category->save()) {
      return true;
    } else {
      $this->validationErrors = $category->errors();
      return false;
    }
//    $validator = Validator::make(Input::all(), $this->rules);
//		if ($validator->passes()) {
//      $category = new Category;
//      $category->name = Input::get('name');
//      $category->desc = Input::get('desc');
//      $category->save();
//			return true;
//		} else {
//      $this->validationErrors = $validator;
//			return false;
//		}
  }

  public function update($id)
  {
    $category = Category::find($id);
    if(is_null($category)) {
      return false;
    } else {
      $category->name = Input::get('name');
      $category->desc = Input::get('desc');
      if($category->updateUniques()) {
        return true;
      } else {
        $this->validationErrors = $category->errors();
        return false;
      }
    }

  }

  public function delete($id)
  {
    $category = Category::find($id);
    if(is_null($category)) {
      return false;
    } else {
      if($category->delete()) {
        return true;
      }
      return false;

    }

  }

  public function errors()
  {
    return $this->validationErrors;
  }

}
