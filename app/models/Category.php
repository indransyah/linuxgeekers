<?php

use LaravelBook\Ardent\Ardent;

class Category extends Ardent {

	public static $rules = [
		'name' => 'required|unique:categories'
	];

	protected $table = 'categories';

	public function product() {
		return $this->hasMany('Product');
	}

}
