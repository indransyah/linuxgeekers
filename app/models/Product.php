<?php

class Product extends Eloquent {

	protected $table = 'products';
	// protected $primaryKey = 'product_id';
	public static $rules = array(
		'name' => 'required|max:100|unique:products,name',
		'price' => 'required|integer',
		'system' => 'required',
		'description' => 'required',
		'status' => 'required'
	);

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function picture() {
		return $this->hasMany('Picture');
	}

	public function order() {
        return $this->belongsToMany('Order', 'order_detail', 'product_id', 'order_id')->withPivot('qty', 'size', 'price', 'subtotal');
    }

    public function size() {
        return $this->belongsToMany('Size', 'product_size', 'product_id', 'size_id')->withPivot('quota');
    }

    public function scopePublished($query) {
    	$query->where('status', '!=', 'draft');
    }

    public function scopeFeatured($query) {
    	$query->join('order_detail','products.id', '=', 'order_detail.product_id')
			->select(DB::raw('products.*, SUM(qty) as sum'))
			->groupBy('products.id')
			->orderBy('sum', 'DESC')
			->where('status', '!=', 'draft')
			->take(4);

		// FIX
		// $products = DB::table('products')
		// 	->join('order_detail','products.id', '=', 'order_detail.product_id')
		// 	->select(DB::raw('products.*, SUM(qty) as sum'))
		// 	->groupBy('products.id')
		// 	->orderBy('sum', 'DESC')
		// 	->get();
    }

    public function scopePreOrder($query) {
    	return $query->where('status', 'pre order')->get();
    }

    public function scopeWithOrder($query) {
    	return $query->join('order_detail','products.id', '=', 'order_detail.product_id')
			->select(DB::raw('products.*, SUM(qty) as sum'))
			->where('status', 'pre order')
			->groupBy('products.id')
			->get();
    }

    public function scopeWithOrderDetail($query) {
    	return $query->join('order_detail','products.id', '=', 'order_detail.product_id')
			->select(DB::raw('size, SUM(qty) as sum'))
			->where('status', 'pre order')
			->groupBy('size')
			->get();
    }

    public function scopeOrderDetail($query, $product_id) {
    	return $query->join('order_detail','products.id', '=', 'order_detail.product_id')
			->select(DB::raw('products.*, subtotal, size, SUM(qty) as total_qty, SUM(subtotal) as total'))
			->groupBy('size')
			->where('products.id', $product_id)
			->get();
    }

}
