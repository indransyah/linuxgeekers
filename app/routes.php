<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'UserHomeController@getIndex');
Route::get('home', 'UserHomeController@getIndex');
// Route::get('login', 'UserHomeController@getLogin');
// Route::post('login', 'UserHomeController@postLogin');
// Route::get('logout', 'UserHomeController@getLogout');
// Route::get('register', 'UserHomeController@getRegister');
// Route::post('register', 'UserHomeController@postRegister');
Route::get('product', 'UserProductController@getIndex');
Route::get('product/pre-order/{id}', 'UserProductController@getPreOrder');
Route::get('product/ready-stock/{id}', 'UserProductController@getReadyStock');
Route::get('product/{slug}', 'UserProductController@getShow');
Route::get('search', 'UserProductController@getSearch');
Route::post('search', 'UserProductController@postSearch');
Route::get('page/{slug}', 'UserPageController@getIndex');
Route::controller('cart', 'UserCartController');
Route::controller('order', 'UserOrderController');
Route::controller('member', 'UserMemberController');


# Customer Routes
// Route::group(['before' => 'customer'], function()
// {
// 	Route::get('checkout', 'UserOrderController@getCheckout');
// 	Route::post('checkout', 'UserOrderController@postCheckout');
// });

# Administrator Routes
Route::group(['prefix' => 'adidev'], function()
{
	Route::controller('user', 'AdminUserController');
	Route::get('login', 'AdminUserController@getLogin');
	Route::get('logout', 'AdminUserController@getLogout');
	Route::post('login', 'AdminUserController@postLogin');

	Route::group(['before' => 'administrator'], function(){
		Route::get('/', 'AdminHomeController@getIndex');
		Route::controller('home', 'AdminHomeController');
		Route::get('page/link/{id}', 'AdminPageController@link');
		Route::get('page/unlink/{id}', 'AdminPageController@unlink');
		Route::resource('page', 'AdminPageController', array('except' => 'show'));
		// Route::get('product/image/{id}', 'AdminProductController@image');
		// Route::post('product/upload', 'AdminProductController@upload');
		Route::controller('product', 'AdminProductController');
		Route::controller('order', 'AdminOrderController');
		Route::controller('pre-order', 'AdminPreOrderController');
		Route::controller('payment', 'AdminPaymentController');
		Route::controller('shipment', 'AdminShipmentController');
		Route::controller('report', 'AdminReportController');
		Route::controller('setting', 'AdminSettingController');
		Route::controller('customer', 'AdminCustomerController');
		Route::controller('category', 'AdminCategoryController');

	});

});



// Route::get('/', 'UserHomeController@getIndex');
// Route::get('login', 'UserHomeController@getLogin');
// Route::post('login', 'UserHomeController@postLogin');
// Route::get('register', 'UserHomeController@getRegister');
// Route::post('register', 'UserHomeController@postRegister');
// Route::get('home', 'UserHomeController@getIndex');
// Route::controller('order', 'UserOrderController');

// Route::group(array('prefix' => 'adidev'), function(){
// 	Route::controller('user', 'AdminUserController');
// });

// Route::group(array('before' => 'auth', 'prefix'=>'adidev'), function(){
// 	Route::get('/', 'AdminHomeController@getIndex');
// 	Route::controller('home', 'AdminHomeController');
// 	Route::resource('page', 'AdminPageController', array('except' => 'show'));
// 	Route::get('product/image/{id}', 'AdminProductController@image');
// 	Route::post('product/upload', 'AdminProductController@upload');
// 	Route::resource('product', 'AdminProductController', array('except' => 'show'));
// 	Route::controller('order', 'AdminOrderController');
// });
// Route::get('product', 'UserProductController@getIndex');
// Route::get('product/{slug}', 'UserProductController@getShow');
// Route::get('search', 'UserProductController@getSearch');
// Route::post('search', 'UserProductController@postSearch');
// Route::get('page/{slug}', 'UserPageController@getIndex');

// Display all SQL executed in Eloquent
// Event::listen('illuminate.query', function($query)
// {
//     var_dump($query);
// });

App::bind('CategoryInterface', 'CategoryRepository');
