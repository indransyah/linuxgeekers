<?php

interface CategoryInterface {

  public function all();

  public function find($id);

  public function store();

  public function update($id);

  public function delete($id);

}
