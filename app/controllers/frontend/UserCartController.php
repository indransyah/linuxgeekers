<?php

class UserCartController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function __construct()
	{
		$this->beforeFilter('customer', array('only' => array('getCheckout', 'postCheckout')));
	}

	public function getIndex()
	{
		$this->getView();
	}

	public function getView()
	{
		Helpers::checkCart();
		$cart = Cart::content();
		$this->layout->content = View::make('frontend.cart.index');
	}

	public function getClear() {
		Cart::destroy();
		return Redirect::action('UserProductController@getIndex');
	}

	public function getRemove($rowId) {
		Cart::remove($rowId);
		return Redirect::action('UserCartController@getView')
			->with('success', 'A product was removed from your cart');
	}

	public function postUpdate() {
		$cart = Cart::content();
		foreach($cart as $row){
			$qty = Input::get($row->rowid.'-qty');
			$product = Product::find($row->id);
			if ($product->system == 'ready stock' && $product->size->count() != 0) {
				if ($product->size()->where('value', $row->options->size)->first()->pivot->quota - $qty >= 0) {
					Cart::update($row->rowid, $qty);
				} else {
					return Redirect::action('UserCartController@getView')
						->with('error', 'Jumlah produk yang tersedia tidak mencukupi.');
				}
			} else {
				Cart::update($row->rowid, $qty);
			}

		}
		return Redirect::action('UserCartController@getView')
			->with('success', 'Cart was updated');
	}

	public function postAdd($slug) {
		$product = Product::where('slug', $slug)->with('picture')->first();
		if (Cart::total() != 0) {
			foreach (Cart::content() as $cart) {
				$tmp = Product::find($cart->id);
				/* Periksa apakah cart memiliki produk dengan sistem order yang sama */
				if ($tmp->system != $product->system) {
					return Redirect::action('UserProductController@getShow', $slug)
						->with('error', 'Cart tidak dapat diisi dengan produk yang memiliki sistem order (pre order / ready stock) yang berbeda.');
				}
			}
		}
		Cart::associate('Product')->add($product->id, $product->name, 1, $product->price, array('size' => Input::get('size')));
		return Redirect::action('UserCartController@getView');
	}

	public function getCheckout() {
		Helpers::checkCart();
		if (Cart::total()==0) {
			return Redirect::action('UserCartController@getView');
		}
		$cart = Cart::content();
		$recipients = Recipient::where('user_id', Sentry::getUser()->id)->lists('name', 'id');
		$recipients = array_add($recipients, 'new', 'New');
		$this->layout->content = View::make('frontend.cart.checkout')
			->with('recipients', $recipients);
	}

	public function getUnique() {
		if (Invoice::where('code', 'dfd')->exists()) {
			return 'benar';
		} else {
			return 'salah';
		}
		$random = str_random(5);
		$unique = false;
		while ($unique == false) {
			$unique = Invoice::where('code', $random)->get()->first();
			return "Ok";
			$random = str_random(5);
		}
	}

	public function postCheckout() {
		if (!Input::get('recipient') || Input::get('recipient') == 'new') {
			$rules = array(
				'name' => 'required|max:50',
				'phone' => 'required|digits_between:11,12',
				'address' => 'required',
				'city' => 'required',
				'province' => 'required',
				'zip' => 'required|digits:5',
				'courier' => 'required',
			);
		} else {
			$rules = array();
		}
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::action('UserCartController@getCheckout')
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
			} else {
				$order = new Order;
				$order->date = date("Y-m-d H:i:s");
				$order->total = Cart::total();
				$order->message = Input::get('message');
				$order->user_id = Sentry::getUser()->id;
				$order->status = 'belum dikonfirmasi';
				$order->save();

				$invoice = new Invoice;
				do {
					$random = str_random(5);
				} while (Invoice::where('code', $random)->exists());

				$invoice->code = $random;
				$invoice->order_id = $order->id;
				$invoice->amount = Cart::total();
				$invoice->save();

				// $payment = new Payment;
				// $payment->invoice_id = $invoice->id;
				// $payment->status = 'belum lunas';
				// $payment->save();

				foreach (Cart::content() as $row) {
					$order->product()->attach($row->id, array('qty' => $row->qty, 'size' => $row->options->size, 'price' => $row->price, 'subtotal' => $row->subtotal));
					$product = Product::find($row->id);
					if($product->system == 'ready stock') {
						$order->type = 'ready stock';
						$order->save();
					} elseif($product->system == 'pre order') {
						$order->type = 'pre order';
						$order->save();
					}
					if ($product->system == 'ready stock' && $product->size->count() != 0) {
						$qty = $product->size()->where('value', $row->options->size)->first()->pivot->quota - $row->qty;
						$size = Size::where('value', $row->options->size)->first();
						if ($qty == 0) {
							$product->size()->detach($size->id);
						} else {
							$product->size()->updateExistingPivot($size->id, array('quota' => $qty));
						}
					}

				}

				if (!Input::get('recipient') || Input::get('recipient') == 'new') {
					$recipient = new Recipient;
					$recipient->name = Input::get('name');
					$recipient->phone = Input::get('phone');
					$recipient->address = Input::get('address');
					$recipient->city = Input::get('city');
					$recipient->province = Input::get('province');
					$recipient->zip_code = Input::get('zip');
					$recipient->user_id = Sentry::getUser()->id;
					$recipient->save();
				}



				$shipment = new Shipment;
				$shipment->courier = Input::get('courier');
				$shipment->message = Input::get('note');
				$shipment->order_id = $order->id;
				if (!Input::get('recipient') || Input::get('recipient') == 'new') {
					$shipment->recipient_id = $recipient->id;
				} else {
					$shipment->recipient_id = Input::get('recipient');
				}
				$shipment->save();

				Cart::destroy();
				return Redirect::action('UserMemberController@getHistory')->with('success', 'Terima kasih telah melakukan pemesanan (Order). Pemesanan Anda sedang kami proses. Silakan tunggu email dari kami untuk mendapatkan tagihan beserta biaya pengiriman barang.');
		}

	}

}
