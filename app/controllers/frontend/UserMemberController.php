<?php

class UserMemberController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function __construct() {
        $this->beforeFilter('customer', array(
        	'except' => array(
        		'getLogin',
        		'postLogin',
        		'getRegister',
        		'postRegister',
						'getLogout',
						'getReset',
						'postReset',
						'getValidate',
						'postValidate',
						'getFacebook',
						'getTwitter',
        	)
        ));
    }

		public function getTwitter() {
			// get data from input
			$token = Input::get( 'oauth_token' );
			$verify = Input::get( 'oauth_verifier' );

			// get twitter service
			$tw = OAuth::consumer( 'Twitter' );

			// check if code is valid

			// if code is provided get user data and sign in
			if ( !empty( $token ) && !empty( $verify ) ) {

				// This was a callback request from twitter, get the token
				$token = $tw->requestAccessToken( $token, $verify );

				// Send a request with it
				$result = json_decode( $tw->request( 'account/verify_credentials.json' ), true );

				$message = 'Your unique Twitter user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
				echo $message. "<br/>";

				//Var_dump
				//display whole array().
				dd($result);

			}
			// if not ask for permission first
			else {
				// get request token
				$reqToken = $tw->requestRequestToken();

				// get Authorization Uri sending the request token
				$url = $tw->getAuthorizationUri(array('oauth_token' => $reqToken->getRequestToken()));

				// return to twitter login url
				return Redirect::to( (string)$url );
			}
		}

		public function getFacebook() {

			// get data from input
			$code = Input::get( 'code' );

			// get fb service
			$fb = OAuth::consumer( 'Facebook' );

			// check if code is valid

			// if code is provided get user data and sign in
			if ( !empty( $code ) ) {

				// This was a callback request from facebook, get the token
				$token = $fb->requestAccessToken( $code );

				// Send a request with it
				$result = json_decode( $fb->request( '/me' ), true );

				try
				{
					$user = Sentry::findUserByLogin($result['email']);
					Sentry::login($user, false);
					return Redirect::intended('product')->with('success', 'Welcome to Linux Geekers');
				}
				catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
				{
					$user = Sentry::createUser(array(
						'name'      => $result['name'],
						'email'     => $result['email'],
						'password'  => str_random(12),
						'activated' => true,
						));
						// Find the group using the group id
						$customerGroup = Sentry::findGroupByName('Customer');
						// Assign the group to the user
						$user->addGroup($customerGroup);

						Sentry::login($user, false);
						return Redirect::intended('product')->with('success', 'Welcome to Linux Geekers');
				}
			}
			// if not ask for permission first
			else {
				// get fb authorization
				$url = $fb->getAuthorizationUri();

				// return to facebook login url
				return Redirect::to( (string)$url );
			}
		}

    public function getReset() {
        $this->layout->content = View::make('frontend.reset');
    }

    public function postReset() {
        $rules = array(
            'email' => 'required|email'
            );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::action('UserMemberController@getReset')
                ->with('error', '')
                ->withErrors($validator)
                ->withInput();
        }

        try
        {
            // Find the user using the user email address
            $user = Sentry::findUserByLogin(Input::get('email'));

            // Get the password reset code
            $resetCode = $user->getResetPasswordCode();

            // Now you can send this code to your user via email for example.
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Redirect::action('UserMemberController@getReset')
                ->with('error', 'Not registered email address.');
        }

        Event::fire('reset.password', array(Input::get('email'), $resetCode));
        return Redirect::action('UserMemberController@getReset')
            ->with('success', 'Please check your email address!');

    }

    public function getValidate($resetCode) {
        $this->layout->content = View::make('frontend.validate', compact('resetCode'));
    }

    public function postValidate($resetCode) {
        $rules = array(
            'password' =>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation' =>'required|alpha_num|between:6,12'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::action('UserMemberController@getValidate', $resetCode)
                ->with('error', '')
                ->withErrors($validator)
                ->withInput();
        }
        $tmpUser = User::where('reset_password_code', $resetCode)->first();
        if (is_null($tmpUser)) {
            return Redirect::action('UserMemberController@getReset')
                ->with('error', 'Password reset code is invalid. Please reset your password again!');
        }
        $id = $tmpUser->id;

        try
        {
            // Find the user using the user id
            $user = Sentry::findUserById($id);

            // Check if the reset password code is valid
            if ($user->checkResetPasswordCode($resetCode))
            {
                // Attempt to reset the user password
                if ($user->attemptResetPassword($resetCode, Input::get('password')))
                {
                    // Password reset passed
                    return Redirect::action('UserMemberController@getLogin')
                        ->with('success', 'Now you can login with your new password.');
                }
                else
                {
                    // Password reset failed
                    return Redirect::action('UserMemberController@getReset')
                        ->with('error', 'Ups, sorry. Something wrong, please reset your password again!');
                }
            }
            else
            {
                // The provided password reset code is Invalid
                return Redirect::action('UserMemberController@getReset')
                    ->with('error', 'Password reset code is invalid.');
            }
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Redirect::action('UserMemberController@getReset')
                ->with('error', 'Not registered email address.');
        }
    }

    public function getLogin() {
    	if(Sentry::check()) {
    		if(Sentry::getUser()->hasAccess('customer')) {
    			return Redirect::action('UserProductController@getIndex')
    				->with('success', 'You are already logged in!');
    		} else {
    			$this->layout->content = View::make('frontend.login');
    		}
    	} else {
    		$this->layout->content = View::make('frontend.login');
    	}
    }

	public function postLogin() {
		try {
    		// Login credentials
			$credentials = array(
				'email'    => Input::get('email'),
				'password' => Input::get('password'),
				);
    		// Authenticate the user
			$user = Sentry::authenticate($credentials, false);
            return Redirect::intended('/')->with('success', 'Welcome to Linux Geekers');
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::action('UserMemberController@getLogin')
                ->with('error', 'Login field is required.')->withInput();
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::action('UserMemberController@getLogin')
                ->with('error', 'Password field is required.')->withInput();
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::action('UserMemberController@getLogin')
                ->with('error', 'Wrong password, try again.')->withInput();
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::action('UserMemberController@getLogin')
                ->with('error', 'User was not found.')->withInput();
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::action('UserMemberController@getLogin')
                ->with('error', 'User is not activated.')->withInput();
        }
	}

	public function getRegister() {
		if(Sentry::check()) {
            $user = Sentry::findUserByID(Sentry::getUser()->id);
            if($user->hasAccess('customer')) {
                return Redirect::action('UserProductController@getIndex')
    				->with('success', 'You are already registered!');
            } else {
			$this->layout->content = View::make('frontend.register');
            }
        } else {
			$this->layout->content = View::make('frontend.register');
        }
	}

	public function postRegister() {
        $rules = array(
            'name' => 'required|max:50',
            'email' =>'required|email|max:45|unique:users,email',
            'password' =>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation' =>'required|alpha_num|between:6,12'
        );

		$validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
        	// Create the user
        	$user = Sentry::createUser(array(
        		'name'      => Input::get('name'),
        		'email'     => Input::get('email'),
        		'password'  => Input::get('password'),
        		'activated' => true,
        		));
            // Find the group using the group id
        	$customerGroup = Sentry::findGroupByName('Customer');
            // Assign the group to the user
        	$user->addGroup($customerGroup);
            return Redirect::action('UserMemberController@getLogin')
            	->with('success', 'Congratulation! Now you can login with your account.');
        } else {
            return Redirect::action('UserMemberController@getRegister')
            	->with('error', 'The following errors occurred')
            	->withErrors($validator)
            	->withInput();
        }
	}

	public function getLogout() {
		Sentry::logout();
		return Redirect::action('UserMemberController@getLogin')
			->with('error', 'Your are now logged out!');
	}

    public function getIndex() {
    	$order = Order::where('user_id', Sentry::getUser()->id)->orderBy('date', 'desc')->get();
        $this->layout->content = View::make('frontend.member.index')
            ->with('order', $order);
    }


	public function getHistory() {
        $this->getIndex();
	}

	public function getInvoice($id) {
		$invoice = Invoice::where('code', $id)->get()->first();
        // return $invoice->payment;
        // if ($invoice->payment) {
        //     return 'ok';
        // } else {
        //     return 'nope';
        // }
        $this->layout->content = View::make('frontend.member.show')
            ->with('invoice', $invoice);

  //       $order = Order::find($id);
		// $this->layout->content = View::make('frontend.member.show')
		// 	->with('order', $order);

	}

	public function getPayment() {
        $invoice = Invoice::where('status', 0)->whereHas('order', function($q) {
                $q->where('user_id', Sentry::getUser()->id)->where('status', 'telah dikonfirmasi');
            })->lists('code', 'code');
        // return $invoice;

        // $order = Order::where('user_id', Sentry::getUser()->id)->where('status', 'telah dikonfirmasi')->get();
        // return $order->count();
        // if ($order->count() != 0) {
            // $invoice = Invoice::lists('code', 'code');
        $this->layout->content = View::make('frontend.member.payment')
            ->with('invoice', $invoice);
        // } else {
            // $this->layout->content = View::make('frontend.member.payment')
                // ->with('order', $order);
        // }


	}

    public function postPayment() {
        $rules = array(
                'invoice' => 'required',
                'amount' => 'required|integer',
                'message' => 'required'
            );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::action('UserMemberController@getPayment')
                ->with('error', 'The following errors occurred')
                ->withErrors($validator)
                ->withInput();
        } else {
            $invoice = Invoice::where('code', Input::get('invoice'))->get()->first();
            $invoice->status = 1;
            $invoice->save();
            $payment = new Payment;
            $payment->invoice_id = $invoice->id;
            $payment->status = 'proses';
            $payment->amount = Input::get('amount');
            $payment->message = Input::get('message');
            $payment->date = date("Y-m-d H:i:s");
            $payment->save();

            // $invoice = Invoice::where('code', Input::get('invoice'))->get()->first();
            // $invoice->status = 1;
            // $invoice->save();
            // $invoice->payment->amount = Input::get('amount');
            // $invoice->payment->message = Input::get('message');
            // $invoice->payment->date = date("Y-m-d H:i:s");

            // $invoice->payment->save();
            return Redirect::action('UserMemberController@getPayment')
                ->with('success', 'Terima kasih telah melakukan pembayaran. Kami akan memeriksa pembayaran Anda. Anda dapat melihat status pembayaran dan pengiriman barang Anda pada Detail Order pada halaman Member.')
                ->withErrors($validator)
                ->withInput();
        }
    }

	public function getDownload($id) {
		// $pdf = App::make('dompdf');
		// $pdf->loadHTML('<h1>Test</h1>');
		// return $pdf->stream();
		// $order = Order::find($id);
        $company = Company::find(1);
        $invoice = Invoice::where('code', $id)->get()->first();
        // return $company->bbm;
		$pdf = PDF::loadView('frontend.pdf.invoice', compact('invoice', 'company'));
		// $pdf = PDF::loadView('downloadinvoice', compact('order'));
		return $pdf->stream();

		// return $pdf->download('invoice.pdf');
	}

}
