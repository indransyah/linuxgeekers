<?php

class UserProductController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex() {
		$products = Product::published()->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getPreOrder($category_id) {
		$products = Product::published()->where('system', 'pre order')->where('category_id', $category_id)->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products)
			->with('category_id', $category_id);

	}

	public function getReadyStock($category_id) {
		$products = Product::published()->where('system', 'ready stock')->where('category_id', $category_id)->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products)
			->with('category_id', $category_id);
	}

	public function getShow($slug) {
		$product = Product::published()->where('slug', $slug)->with('picture')->first();
		$this->layout->content = View::make('frontend.product.show')
			->with('product', $product);
	}

	public function getSearch() {

	}

	public function postSearch() {
		$products = Product::published()->where('name', 'like', '%'.Input::get('keyword').'%')->with(array('picture' => function($query) {
			$query->first();
		}))->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

}
