<?php

class AdminCustomerController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
    $group = Sentry::findGroupByName('Customer');
    $customers = Sentry::findAllUsersInGroup($group);
    $this->layout->content = View::make('backend.customer.index')
      ->with('customers', $customers);
	}

  public function getShow($id) {
    // $orders = Order::where('user_id', $id)->get();
    $user = User::find($id);
    $orders = $user->order;
    if($orders->count() == 0) {
      return Redirect::action('AdminCustomerController@getIndex')->withError('Pelangan belum pernah melakukan order.');
    }
    $this->layout->content = View::make('backend.customer.order')
			->with('orders', $orders)
      ->with('user', $user);
  }

}
