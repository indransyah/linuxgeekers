<?php

class AdminPreOrderController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		// $products = Product::withOrder();
		// $this->layout->content = View::make('backend.preorder.index')
		// 	->with('products', $products);

		$orders = Order::whereType('pre order')->orderBy('date', 'desc')->get();
		$this->layout->content = View::make('backend.preorder.index')
			->with('orders', $orders);
	}

	public function getShow($id) {
		$order = Order::find($id);
		$this->layout->content = View::make('backend.preorder.show')
			->with('order', $order);

	}

	public function getAddTypeFieldOnOrdersTable() {
		$orders = Order::all();
		foreach($orders as $order) {
			// return $order->product[0]->system;
			if($order->product[0]->system == 'ready stock') {
				$order->type = 'ready stock';
			} else {
				$order->type = 'pre order';
			}
			$order->save();
		}
	}
}
