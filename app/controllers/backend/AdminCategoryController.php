<?php

class AdminCategoryController extends BaseController {

	protected $layout = 'backend.layouts.master';

	protected $category;

	public function __construct(CategoryInterface $category)
	{
		$this->category = $category;
	}

	public function getIndex()
	{
		$categories = $this->category->all();
		$this->layout->content = View::make('backend.category.index', compact('categories'));
	}

	public function getCreate()
	{
		$this->layout->content = View::make('backend.category.create');
	}

	public function postStore()
	{
		if($this->category->store())
		return Redirect::action('AdminCategoryController@getIndex')->with('success', 'Category successfully added!');

		return Redirect::action('AdminCategoryController@getCreate')
			->with('error', 'The following errors occurred')
			->withErrors($this->category->errors())
			->withInput();
		// return Redirect::action('AdminCategoryController@getCreate')->withSuccess('Category tidak dapat ditambahkan.');
		// return Redirect::action('AdminCategoryController@getCreate')->withSuccess('Category berhasil ditambahkan.');
	}

	public function getEdit($id)
	{
		$category = $this->category->find($id);
		if(is_null($category)) return Redirect::action('AdminCategoryController@getIndex')->withError('Wrong Category ID.');
		$this->layout->content = View::make('backend.category.edit', compact('category'));
	}

	public function postUpdate($id)
	{
		if($this->category->update($id)) {
			return Redirect::action('AdminCategoryController@getIndex')->with('success', 'Category successfully updated!');
		} else {
			return Redirect::action('AdminCategoryController@getEdit', $id)
				->with('error', 'The following errors occurred')
				->withErrors($this->category->errors())
				->withInput();
		}
	}

	public function deleteDestroy($id)
	{
		if($this->category->delete($id)){
			return Redirect::action('AdminCategoryController@getIndex')
				->with('success', 'Category successfully deleted!');
		} else {
			return Redirect::action('AdminCategoryController@getIndex')
				->with('error', 'Ups, cannot delete the Category!');
		}

	}

}
