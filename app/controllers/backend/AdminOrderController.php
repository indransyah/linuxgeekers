<?php

class AdminOrderController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getSendInvoice($id) {
		$order = Order::find($id);
		$data['company'] = Company::find(1);
        $data['invoice'] = $order->invoice;
		Mail::send('frontend.pdf.invoice', $data, function($m) use ($order){
			$m->to($order->user->email, $order->user->name);
			$m->subject('Invoice');
		});
		return Redirect::action('AdminOrderController@getShow', $id)
			->with('success', 'Invoice was succesfully sent!');
	}

	public function getIndex() {
		$orders = Order::whereType('ready stock')->orderBy('date', 'desc')->get();
		$this->layout->content = View::make('backend.order.index')
			->with('orders', $orders);

	}

	public function getShow($id) {
		$order = Order::find($id);
		$this->layout->content = View::make('backend.order.show')
			->with('order', $order);
	}

	public function postUpdate($id) {
		$rules = array(
			'extra-cost' => 'integer',
			'shipping-cost' => 'required|integer'
			);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		} else {
			$order = Order::find($id);
			$order->shipment->cost = Input::get('shipping-cost');
			$order->shipment->save();
			$order->extra_cost = Input::get('extra-cost');
			$order->status = Input::get('status');
			$order->save();
			$order->invoice->amount = $order->total + Input::get('extra-cost') + Input::get('shipping-cost');
			$order->invoice->save();
			// Helpers::sendEmail();
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('success', 'Aditional cost was added & invoice was sent.');
		}
	}

	public function postExtraCost($id) {
		$rules = array('extra-cost' => 'required|integer');
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		} else {
			$order = Order::find($id);
			$order->extra_cost = Input::get('extra-cost');
			$order->save();
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('success', 'Extra cost was added.');
		}

	}

	public function postShippingCost($id) {
		$rules = array('shipping-cost' => 'required|integer');
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		} else {
			$order = Order::find($id);
			$order->shipping->cost = Input::get('shipping-cost');
			$order->shipping->save();
			return Redirect::action('AdminOrderController@getShow', $id)
				->with('success', 'Shipping cost was added.');
		}

	}

	public function getInvoice() {
		return $string = str_random(5);
		$order = Order::find(2);
		return View::make('invoice')->with('order', $order);
	}

	public function deleteDestroy($id) {
		$order = Order::find($id);
		/* Mengembalikan qty produk jika order dihapus */
		$products = $order->product;
		foreach ($products as $product) {
			if ($product->system == 'ready stock') {
				$size = $product->pivot->size;
				$sizeid = Size::findIdFromValue($size);
				$oldqty = $product->pivot->qty;
				$check = Product::whereHas('size', function ($q) use ($size) {
					$q->where('value', $size);
				})->count();
				if ($check != 0) {
					if ($product->size()->count() == 0) {
						// Jika produk yang diorder sold out
						$product->size()->attach($sizeid, array('quota' => $product->pivot->qty));
					} else {
						if(is_null($product->size()->where('value', $size)->first())) {
							$product->size()->attach($sizeid, array('quota' => $product->pivot->qty));
						} else {
							// Jika produk yang diorder belum sold out
							$qty = $product->size()->where('value', $size)->first()->pivot->quota;
							$product->size()->updateExistingPivot($sizeid, array('quota' => $oldqty + $qty));
						}

					}
				} else {
					$product->size()->attach($sizeid, array('quota' => $oldqty));
				}
			} elseif ($product->system == 'pre order') {
				# code...
			}
		}

		$order->delete();
		return Redirect::action('AdminOrderController@getIndex')->with('success', 'An order succesfully deleted');
	}

}
