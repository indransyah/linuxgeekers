<?php

class AdminProductController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$products = Product::all();
		$this->layout->content = View::make('backend.product.index')
			->with('products', $products);
	}

	public function getCreate() {
		$this->layout->content = View::make('backend.product.create');
	}

	public function postStore() {
		$validator = Validator::make(Input::all(), Product::$rules);
		if ($validator->passes()) {
			$product = new Product;
			$product->name = Input::get('name');
			$product->slug = Str::slug(Input::get('name'), '-');
			do {
				$random = str_random(8);
			} while (Product::where('sku', $random)->exists());
			$product->sku = $random;
			$product->price = Input::get('price');
			$product->system = Input::get('system');
			$product->description = Input::get('description');
			$product->status = Input::get('status');
			$product->save();

			return Redirect::action('AdminProductController@getEdit', $product->id)
				->with('success', 'Product successfully added! You can upload photos now.');
		} else {
			return Redirect::action('AdminProductController@getCreate')
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		}
	}

	public function getEdit($id) {
		$product = Product::with('picture', 'size')->find($id);
		$pictures = Picture::where('product_id', $id)->get();
		if ($product) {
			$categories = Category::lists('name', 'id');
			$this->layout->content = View::make('backend.product.edit')
				->with('product', $product)
				->with('pictures', $pictures)
				->with('categories', $categories);
		} else {
			return Redirect::action('AdminPageController@getIndex');
		}
	}

	public function postUpdate($id) {
		$rules = Product::$rules;
        $rules['name'] .= ',' . $id . ',id';
        $rules['system'] = '';
		$validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
        	$product = Product::find($id);
			$product->name = Input::get('name');
			$product->slug = Str::slug(Input::get('name'), '-');
			$product->price = Input::get('price');
			$product->category_id = Input::get('category');
			if ($product->system == 'pre order') {
				$product->quota = Input::get('quota');
			} elseif($product->system == 'ready stock') {
				foreach (Size::all() as $size) {
					if (Input::get($size->value) != null || Input::get($size->value) != 0) {
						/* Update tabel product_size sesuai dengan size yang diinputkan */
						if ($product->size()->where('size_id', $size->id)->count() == 0) {
							$product->size()->attach($size->id, array('quota' => Input::get($size->value)));
						} else {
							$product->size()->updateExistingPivot($size->id, array('quota' => Input::get($size->value)));
						}
					} else {
						/* Menghapus relasi dengan size jika inputan size = 0 */
						$product->size()->detach($size->id);
					}
					// Reset
					// if (Input::get($size->value) == 0) {
					// 	$product->size()->detach($size->id);
					// }
				}
			}
			$product->description = Input::get('description');
			$product->status = Input::get('status');
			$product->save();
			return Redirect::action('AdminProductController@getIndex')
				->with('success', 'Product successfully edited!');
        } else {
        	return Redirect::action('AdminProductController@getEdit', $id)
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
        }
	}

	public function deleteDestroy($id) {
		$product = Product::find($id);
		if ($product) {
			$product->delete();
			return Redirect::action('AdminProductController@getIndex')
				->with('success', 'Product successfully deleted!');
		} else {
			return Redirect::action('AdminProductController@getIndex');

		}
	}

	public function postUpload($id) {
		$files = Input::file('files');
		foreach ($files as $file) {
			$picture = new Picture();
			$picture->photo = $file;
			$picture->product_id = $id;
			$picture->save();
			$name = $picture->photo->url('medium');
			$id = $picture->id;
			$results[] = compact('name');
		}
		return array(
			'files' => $results,
			'id' => $id
		);
	}

	public function getDelete($id, $product_id) {
		$picture = Picture::find($id);
		$picture->delete();
		return Redirect::action('AdminProductController@getEdit', $product_id)
			->with('success', 'Successfully delete the image.');

	}

	public function getOrder($id) {
		$product = Product::find($id);
		$detail = Product::orderDetail($id);
		// return $detail;
		$this->layout->content = View::make('backend.product.order')
			->with('product', $product)
			->with('detail', $detail);
	}

	public function getCustomer($id) {
		$product = Product::find($id);
		$recipients = Recipient::whereHas('shipment', function($q) use ($id) {
			$q->where('status', 'packaging')
				->whereHas('order', function($q) use ($id) {
					$q->whereHas('product', function($q) use ($id) {
						$q->where('product_id', $id);
					});
				});
		})->get();
//		return $recipients->count();
		$this->layout->content = View::make('backend.product.customer')
			->with('product', $product)
			->with('recipients', $recipients);
	}

	public function getSummary($id) {
		$product = Product::find($id);
		$detail = Product::orderDetail($id);
		$pdf = PDF::loadView('backend.pdf.summary', compact('product'), compact('detail'));
		return $pdf->stream();
	}

	public function getBulkPrint($id) {
		$recipients = Recipient::whereHas('shipment', function($q) use ($id) {
			$q->where('status', 'packaging')
				->whereHas('order', function($q) use ($id) {
					$q->whereHas('product', function($q) use ($id) {
						$q->where('product_id', $id);
					});
				});
		})->get();
		$pdf = PDF::loadView('backend.pdf.new-recipients', compact('recipients'));
		return $pdf->stream();
	}

	public function getConvertPackagingStatus($product_id)
	{
		$product = Product::find($product_id);
		foreach($product->order as $order) {
			if($order->invoice->payment != null) {
				$order->shipment->status = 'packaging';
				$order->shipment->save();
			}
		}
		return 'Ok';
	}

}
