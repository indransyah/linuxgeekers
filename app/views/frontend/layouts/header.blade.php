	<div class="pull-right" style="margin:0px 5px 0px 0px;color:#fff;">
		@if(Sentry::check())
			@if(Sentry::getUser()->hasAccess('customer'))
				<small>Welcome, <strong>{{Sentry::getUser()->name}}</strong> | <a href="{{ URL::action('UserMemberController@getLogout') }}" style="color:#fff;">Logout</a></small>
			@endif
		@else
			<small><a href="{{ URL::action('UserMemberController@getRegister') }}" style="color:#fff;">Register</a> | <a href="{{ URL::action('UserMemberController@getLogin') }}" style="color:#fff;">Login</a></small>
		@endif
	</div>
	<div class="header">
		<div class="container">
			<div class="row">
			  	<div class="col-md-12">
					<div class="header-left">
						<div class="logo">
							<a href="{{ URL::to('home') }}"><img src="{{ $company->logo->url() }}" style="height:60px;" alt=""/></a>
						</div>
						<div class="menu">
						 	<a class="toggleMenu" href="#"><img src="{{ asset('assets/images/nav.png') }}" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li class="{{ Request::is('product/ready-stock*') ? 'current' : '' }}"><a href="{{ URL::action('UserProductController@getReadyStock', 1) }}">Ready Stock</a></li>
						    	<li class="{{ Request::is('product/pre-order*') ? 'current' : '' }}"><a href="{{ URL::action('UserProductController@getPreOrder', 1) }}">Pre Order</a></li>
						    	@foreach(Page::menu() as $menu)
						    	<li class="{{ Request::is('page/'.$menu->slug) ? 'current' : '' }}"><a href="{{ URL::to('page/'.$menu->slug) }}">{{ $menu->title }}</a></li>
						    	@endforeach
						    	<li class="{{ Request::is('member*') ? 'current' : '' }}"><a href="{{ URL::to('member') }}">Member</a></li>
								<div class="clear"></div>
							</ul>
							{{ HTML::script('assets/js/responsive-nav.js') }}
						</div>
						<div class="clear"></div>
	    	    	</div>

	            	<div class="header_right">
	    		  		<!-- start search-->
				  		<div class="search-box">
							<div id="sb-search" class="sb-search">
            		{{ Form::open(array('action' => 'UserProductController@postSearch')) }}
                	{{ Form::text('keyword', null, array('class' => 'sb-search-input', 'placeholder' => 'Search product...', 'type' => 'search' )) }}
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search"> </span>
								{{ Form::close() }}
							</div>
						</div>
						{{ HTML::script('assets/js/classie.js') }}
						{{ HTML::script('assets/js/uisearch.js') }}
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
				    	<ul class="icon1 sub-icon1 profile_img">
					 		<li><a class="active-icon c1" href="#"> </a>
								<ul class="sub-icon1 list">
								 	<div class="product_control_buttons">
								  		<a href="{{ URL::action('UserCartController@getClear') }}"><img src="{{ asset('assets/images/close_edit.png') }}" alt=""/></a>
								  	</div>
								  	@if(Cart::count() == 0)
									  	<div class="clear"></div>
								  		<p> Empty cart </p>
								  	@else
									  	@foreach(Cart::content() as $content)
									  	<div class="clear"></div>
									  	<li>
									  		<strong>{{ $content->name }}</strong><br />
									  		<span class="actual">{{ $content->qty }} x {{ Helpers::rupiah($content->price) }}</span>
									  	</li>
									  	@endforeach
								  	@endif
								  	<div class="login_buttons">
										<div class="login_button"><a href="{{ URL::action('UserCartController@getView') }}">View All</a></div>
								  	</div>
								</ul>
					 		</li>
						</ul>
		        		<div class="clear"></div>
	       			</div> <!-- End of header_right -->
	      		</div> <!-- End of col -->
			</div> <!-- End of row -->
	    </div> <!-- End of Container -->
	</div> <!-- End of Header -->
