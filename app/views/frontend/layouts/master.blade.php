<!DOCTYPE HTML>
<html>
<head>
<title>{{ $company->name }}</title>
{{ HTML::style('assets/css/bootstrap.css') }}
{{ HTML::style('assets/css/style.css') }}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta name="Description" content="{{ $company->description }}" />
<meta name="Keywords" content="linux, distro, pre order, ready stock, kaos, polo, jaket" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
{{ HTML::script('assets/js/jquery.min.js') }}
{{ HTML::style('assets/css/fwslider.css') }}
{{ HTML::script('assets/js/jquery-ui.min.js') }}
{{ HTML::script('assets/js/fwslider.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });

            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });

            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
</head>
<body>
	@include('frontend.layouts.header')

	@yield('content')

	@include('frontend.layouts.footer')
</body>
</html>
