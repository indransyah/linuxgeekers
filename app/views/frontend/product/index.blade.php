@extends('frontend.layouts.master')
@section('content')
	<div class="main">
		<div class="shop_top" style="padding: 2% 0px;">
			<div class="container">
      			@include('frontend.layouts.alert')
				<div class="row" style="margin-bottom: 25px;">
					<div class="col-md-12">
						Categories :
						@foreach(Category::all() as $category)
						@if(Request::is('product/ready-stock*'))
						<a style="color: #000;" href="{{ URL::action('UserProductController@getReadyStock', $category->id) }}">
						@else
						<a style="color: #000;" href="{{ URL::action('UserProductController@getPreOrder', $category->id) }}">
						@endif
							{{ ($category->id == $category_id) ? '<strong style="color:#F00;">' : '' }}
							{{ $category->name }}
							{{ ($category->id == $category_id) ? '</strong>' : '' }}
						</a>
						|
						@endforeach
					</div>
				</div>
				<div class="row shop_box">
					@if($products->count() > 0)
					@foreach($products as $product)
					<div class="col-md-3 shop_box-top">
						<a href="{{ URL::to('product/'.$product->slug) }}">
							<img style="width:300px;height:320px;border: 1px solid #999;" src="{{ ($product->picture->count() == 0) ? URL::to('images/default-product.jpg') : URL::to($product->picture->first()->photo->url()) }}" class="img-responsive" alt=""/>
							<!-- <span class="new-box">
								<span class="new-label">New</span>
							</span> -->
							<!-- <span class="sold-out">
								<span class="sold-out-label">SOLD OUT</span>
							</span> -->
							<!-- <span class="pre-order">
								<span class="pre-order-label">PRE ORDER</span>
							</span> -->
							<!-- <span class="ready-stock">
								<span class="ready-stock-label">READY STOCK</span>
							</span> -->
							<div class="shop_desc" style="background-color:#111;border: 1px solid #111;">
								<h3><a href="#" style="color:#FFF;">{{ $product->name }}</a></h3>
								<p style="color:#FFF;">{{ strtoupper($product->system) }} </p>
								<span class="actual" style="color:#FFF;">{{ Helpers::rupiah($product->price) }}</span><br>
								<!-- <ul class="buttons"> -->
									<!-- <li class="cart"></li> -->
									<!-- <li class="shop_btn"><a href="{{ URL::to('product/'.$product->slug) }}">Read More</a></li> -->
									<!-- <div class="clear"> </div> -->
							    <!-- </ul> -->
						    </div>
						</a>
					</div>
					@endforeach
					@else
					<h4>No product</h4>
					@endif
					<!-- <div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic10.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<span class="sale-box">
								<span class="sale-label">Sale!</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic11.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="reducedfrom">$66.00</span>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic12.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<span class="sale-box">
								<span class="sale-label">Sale!</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="reducedfrom">$66.00</span>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div> -->
				</div>
				<!-- <div class="row">
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic9.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic10.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<span class="sale-box">
								<span class="sale-label">Sale!</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic11.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="reducedfrom">$66.00</span>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
					<div class="col-md-3 shop_box">
						<a href="single.html">
							<img src="{{ asset('assets/images/pic12.jpg') }}" class="img-responsive" alt=""/>
							<span class="new-box">
								<span class="new-label">New</span>
							</span>
							<span class="sale-box">
								<span class="sale-label">Sale!</span>
							</span>
							<div class="shop_desc">
								<h3><a href="#">aliquam volutp</a></h3>
								<p>Lorem ipsum consectetuer adipiscing </p>
								<span class="reducedfrom">$66.00</span>
								<span class="actual">$12.00</span><br>
								<ul class="buttons">
									<li class="cart"><a href="#">Add To Cart</a></li>
									<li class="shop_btn"><a href="#">Read More</a></li>
									<div class="clear"> </div>
							    </ul>
						    </div>
						</a>
					</div>
				</div> -->
		 	</div>
		</div>
	</div>
@stop
