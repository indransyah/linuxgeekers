@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    View Customers
  </h1>
</section>
<!-- Main content -->
<section class="content">
  @include('backend.layouts.alert')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
      <div class="box-body table-responsive">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th width="30%">Name</th>
              <th>Email</th>
              <th>Order</th>
              <th width="5%">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($customers as $customer)
            <tr>
              <td>{{ $customer->id }}</td>
              <td>{{ $customer->name }}</td>
              <td>{{ $customer->email }}</td>
              <td>{{ Order::whereUserId($customer->id)->count(); }}</td>
              <td>
                <a href="{{ URL::action('AdminCustomerController@getShow', $customer->id) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-fw fa-eye"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div>
</section><!-- /.content -->
@stop()
