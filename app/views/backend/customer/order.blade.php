@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    View Orders by {{ $user->name }}
  </h1>
</section>
<!-- Main content -->
<section class="content">
  @include('backend.layouts.alert')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body table-responsive">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th width="20%">Date</th>
                <th width="10%">Invoice</th>
                <th>Name</th>
                <th>Total</th>
                <th>Status</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr class="{{ ($order->status == 'telah dikonfirmasi') ? 'success' : 'danger' }}">
                <td>{{ Helpers::date($order->date) }}</td>
                <td>{{ strtoupper($order->invoice->code) }}</td>
                <td>{{ $order->user->name }}</td>
                <td>{{ Helpers::rupiah($order->total) }}</td>
                <td>{{ strtoupper($order->status) }}</td>
                <td>
                  <div class="btn-group">
                    <a href="{{ URL::action('AdminOrderController@getShow', $order->id) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-fw fa-eye"></i></a>
                    <a class="btn btn-sm btn-danger btn-flat" data-toggle="modal" data-target="#deleteModal-{{ $order->id }}" data-toggle="tooltip" data-placement="right" title="Delete order"><i class="fa fa-fw fa-trash-o"></i></a>
                    <div class="modal fade" id="deleteModal-{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure to delete invoice with code <strong>{{ strtoupper($order->invoice->code) }}</strong> from your database ?
                          </div>
                          <div class="modal-footer">
                            {{ Form::open(array('action' => array('AdminOrderController@deleteDestroy', $order->id), 'method'=>'DELETE')) }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                            {{ Form::close() }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section><!-- /.content -->
@stop()
