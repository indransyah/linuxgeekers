<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title>Invoice - Linux Geekers</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        {{ HTML::style('assets/css/bootstrap.css') }}
        <!-- CUSTOM STYLE  -->
        {{ HTML::style('assets/invoice/css/custom-style.css') }}
        <!-- <link href="assets/css/custom-style.css" rel="stylesheet" /> -->
        <!-- GOOGLE FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />

      </head>
      <body>
       <div class="container">

        <div class="row pad-top-botm ">
         <div class="col-lg-6 col-md-6 col-sm-6 ">
          <img src="{{ asset('assets/images/lg.png') }}" style="padding-bottom:20px;" />
          <span style="font-family: 'Kaushan Script', cursive;">LINUX GEEKERS</span>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
         <strong>Linux Geekers</strong>
         <br />
         <i>Address :</i> Jl.Imogiri Barat Km 12 Bantul
         <br />
         Yogyakarta

       </div>
     </div>
     <div  class="row text-center contact-info">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <hr />
         <span>
           <strong>Email : </strong>  order@linuxgeekers.com
         </span>
         <span>
           <strong>Phone : </strong>  085729009963
         </span>
         <span>
           <strong>BBM : </strong>  7CB2DABD
         </span>
         <hr />
       </div>
     </div>
     <div  class="row pad-top-botm client-info">
       <div class="col-lg-4 col-md-4 col-sm-6">
         <h4>  <strong>Shipped to</strong></h4>
         <?php $order = Order::find(3); ?>
         <strong>{{ $order->shipment->recipient->name }}</strong>
         <br />
         <b>Address :</b> {{ $order->shipment->recipient->address }}
         <br />
         {{ $order->shipment->recipient->city }} , {{ $order->shipment->recipient->province }}
         <br /> 
         <b>Phone :</b> {{ $order->shipment->recipient->phone }}
         <br />
         <!-- <b>E-mail :</b> info@clientdomain.com -->
       </div>
       <div class="col-lg-4 col-md-4 col-sm-6">
         <h4>  <strong>Shippment Info</strong></h4>
         <b>Courier : {{ strtoupper($order->shipment->courier) }} </b>

         <br />
         <b>Cost :</b> {{ Helpers::rupiah($order->shipment->cost) }}
         <br />
         <b>Weight :</b> (not yeet)
         <br /> 
       </div>
       <div class="col-lg-4 col-md-4 col-sm-6">

         <h4>  <strong>Order Details </strong></h4>
         <b>Total : {{ Helpers::rupiah($order->total + $order->extra_cost + $order->shipment->cost) }} </b>
         <br />
         Date :  {{ Helpers::date($order->date) }}
         <br />
         <b>Payment Status :  (not yet) </b>
         <br />
       </div>
     </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>Product</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Size</th>
                <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
              @foreach($order->product as $product)
              <tr>
                <td>{{ $product->name }}</td>
                <td>{{ Helpers::rupiah($product->pivot->price) }}</td>
                <td>{{ $product->pivot->qty }}</td>
                <td>{{ $product->pivot->size }}</td>
                <td>{{ Helpers::rupiah($product->pivot->subtotal) }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <hr />
        <div class="ttl-amts">
         <h5> Sub Total : {{ Helpers::rupiah($order->total) }} </h5>
       </div>
       <!-- <hr /> -->
       <div class="ttl-amts">
        <h5>  Extra Cost : {{ Helpers::rupiah($order->extra_cost) }} </h5>
      </div>
      <!-- <hr /> -->
      <div class="ttl-amts">
        <h5>  Shipping Cost : {{ Helpers::rupiah($order->shipment->cost) }} </h5>
      </div>
      <hr />
      <div class="ttl-amts">
        <h4> <strong>Total : {{ Helpers::rupiah($order->total + $order->extra_cost + $order->shipment->cost) }}</strong> </h4>
      </div>
    </div>
  </div>
  <div class="row">
   <div class="col-lg-4 col-md-4 col-sm-4 text-center">
    <img src="{{ asset('assets/images/bank_bca.jpg') }}" class="col-lg-6 col-md-4 col-sm-4"> <br />
    <strong> BCA </strong>
    <p>0373323672</p>
    <p>Anugerah Chandra Utama</p>
 </div>
 <div class="col-lg-4 col-md-4 col-sm-4 text-center">
    <img src="{{ asset('assets/images/bank_mandiri.jpg') }}" class="col-lg-6 col-md-4 col-sm-4"> <br />
  <strong> Mandiri </strong>
    <p>137-00-0751607-9</p>
    <p>Anugerah Chandra Utama</p>
</div>
<div class="col-lg-4 col-md-4 col-sm-4">
  <strong> Catatan: </strong>
  <small><i>
    <p>Silakan melakukan pembayaran pada salah satu bank di samping ini.</p>
    <p>Setelah melakukan pembayaran silakan konfirmasi pembayaran Anda pada link berikut ini.</p>
  </i></small>
  <!-- <ol>
    <li>
      This is an electronic generated invoice so doesn't require any signature.

    </li>
    <li>
     Please read all terms and polices on  www.yourdomaon.com for returns, replacement and other issues.

   </li>
 </ol> -->
</div>
</div>
</div>

</body>
</html>
