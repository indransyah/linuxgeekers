@extends('backend.layouts.master')
@section('content')

{{ HTML::style('assets/backend/jQuery-File-Upload/css/jquery.fileupload.css') }}

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Product
                        <!-- <small>Control panel</small> -->
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box'>
                                <div class='box-body pad'>
                                    {{ Form::model($product, array('action' => array('AdminProductController@postUpdate', $product->id), 'files' => 'true')) }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Product\'s name' )) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Price</label>
                                            {{ Form::text('price', null, array('class' => 'form-control', 'placeholder' => 'Product\'s price' )) }}
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            {{ Form::textarea('description', null, array('rows' => '10', 'cols' => '80' )) }}
                                        </div>

                                        @if($product->system == 'pre order')
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            {{ Form::text('quota', null, array('class' => 'form-control', 'placeholder' => 'Product\'s quantity' )) }}
                                            <small><i>Pre order quota</i></small>
                                        </div>
                                        @elseif($product->system == 'ready stock')
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <div class="row">
                                                @foreach(Size::all() as $size)
                                                <div class="col-xs-3" style="margin-bottom: 5px;">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">{{ $size->name }}</span>
                                                        @if($product->size()->where('value', $size->value)->count() > 0)
                                                        {{ Form::text($size->value, $product->size()->where('value', $size->value)->first()->pivot->quota, array('class' => 'form-control')) }}
                                                        @else
                                                        {{ Form::text($size->value, null, array('class' => 'form-control')) }}
                                                        @endif
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <small><i>Leave blank for empty quantity</i></small>

                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <label>Category</label>
                                            {{ Form::select('category', $categories, $product->category_id, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            {{ Form::select('status', array('avaliable' => 'Avaliable', 'draft' => 'Draft', 'sold out' => 'Sold Out'), null, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Photos</label>
                                            <br>
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn btn-success fileinput-button">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>Select files...</span>
                                                <!-- The file input field used as target for the file upload widget -->
                                                <input id="fileupload" type="file" name="files[]" multiple>
                                            </span>
                                            <br>
                                            <br>
                                            <!-- The global progress bar -->
                                            <div id="progress" class="progress">
                                                <div class="progress-bar progress-bar-success"></div>
                                            </div>
                                            <!-- The container for the uploaded files -->
                                            <div id="files" class="files row">
                                                @foreach($pictures as $picture)
                                                <div id="row{{ $picture->id }}" style="margin-bottom: 10px;" class="col-md-3">
                                                    <img style="width:100%;height:300px;" src="{{ $picture->photo->url() }}">
                                                    <a class="btn btn-flat btn-danger btn-block" href="{{ URL::action('AdminProductController@getDelete', [ $picture->id, $product->id ]) }}" type="button" id="delete3">DELETE <i class="fa fa-fw fa-trash-o"></i></a>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="box-footer clearfix">
                                            <button class="pull-right btn btn-info btn-flat" type="submit">Edit <i class="fa fa-edit"></i></button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div><!-- /.box -->
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
                <script type="text/javascript">
                $(function() {
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('description');
                    });
                </script>

{{ HTML::script('assets/backend/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}
{{ HTML::script('assets/backend/jQuery-File-Upload/js/jquery.iframe-transport.js') }}
{{ HTML::script('assets/backend/jQuery-File-Upload/js/jquery.fileupload.js') }}
<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '{{ URL::action('AdminProductController@postUpload', $product->id) }}';
    $('#fileupload').fileupload({
        url: url,
        add: function(e, data) {
                var uploadErrors = [];
                // var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;

                if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('Not an accepted file type');
                }
                if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                    uploadErrors.push('Filesize is too big');
                }
                if(uploadErrors.length > 0) {
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                // $('<p/>').text(file.name).appendTo('#files');
                // $('<img/>').attr('class', 'col-md-3').attr('src', file.name).appendTo('#files');

                $('<div/>').attr('class', 'col-md-3').attr('style', 'margin-bottom: 10px;').attr('id', 'row'+data.result.id).appendTo('#files');
                $('<img/>').attr('src', file.name).attr('style', 'width:100%;height:300px;').appendTo('#row'+data.result.id);
                // $('<div/>').attr('class', 'btn-group').attr('id', 'group'+data.result.id).appendTo('#row'+data.result.id);
                $('<a/>').attr('id', 'delete'+data.result.id).attr('type', 'button').attr('href', '{{ URL::action('AdminProductController@getDelete') }}/'+data.result.id+'/{{ $product->id }}').attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#row'+data.result.id);
                $('<i/>').attr('class', 'fa fa-fw fa-trash-o').appendTo('#delete'+data.result.id);
                // $('<a/>').attr('type', 'button').attr('href', '{{ URL::action('AdminSettingController@getDelete') }}/'+data.result.id).attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#group'+data.result.id);
                // $('<a/>').attr('type', 'button').attr('href', '{{ URL::action('AdminSettingController@getDelete') }}/'+data.result.id).attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#group'+data.result.id);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
@stop()
