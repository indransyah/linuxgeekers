@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Customers <small>SKU : <strong>{{ strtoupper($product->sku) }}</strong> ({{ $product->name }})</small>
                        @if($recipients->count() > 0)
                        <span class="pull-right">
                            <a target="blank" href="{{ URL::action('AdminProductController@getBulkPrint', $product->id) }}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Print address of recipients when shipment's status is 'packaging'"><i class="fa fa-print"></i> Print Recipient's Address</a>
                        </span>
                        @endif
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="20%">Date</th>
                                                <th width="10%">Invoice</th>
                                                <th>Name</th>
                                                <!-- <th>Total</th> -->
                                                <th>Status</th>
                                                <th width="12%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($product->order as $order)
                                            <tr class="{{ ($order->shipment->status == 'dikirim') ? 'success' : 'danger' }}">
                                                <td>{{ Helpers::date($order->date) }}</td>
                                                <td>{{ strtoupper($order->invoice->code) }}</td>
                                                <td>{{ $order->user->name }}</td>
                                                <td>{{ strtoupper($order->shipment->status) }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{ URL::action('AdminOrderController@getShow', $order->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="top" title="View customer order detail"><i class="fa fa-fw fa-shopping-cart"></i></a>
							@if($order->invoice->payment != null)
                                                        <a href="{{ URL::action('AdminPaymentController@getShow', $order->invoice->payment->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="bottom" title="View customer payment detail"><i class="fa fa-fw fa-usd"></i></a>
                                                        @endif
							<a href="{{ URL::action('AdminShipmentController@getShow', $order->shipment->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="top" title="View shipment detail"><i class="fa fa-fw fa-truck"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 
