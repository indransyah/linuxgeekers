@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Products
                        <!-- <small>Control panel</small> -->
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="10%">SKU</th>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Category</th>
                                                <th>Ordered</th>
                                                <th>System</th>
                                                <th width="16%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                            <tr>
                                                <td>{{ strtoupper($product->sku) }}</td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ Helpers::rupiah($product->price) }}</td>
                                                <td>{{ (is_null($product->category)) ? '-' : $product->category->name }}</td>
                                                <td>{{ ($product->system == 'pre order') ? Product::orderDetail($product->id)->sum('total_qty').' of '.$product->quota : Product::orderDetail($product->id)->sum('total_qty') }}</td>
                                                <td>{{ strtoupper($product->system) }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{ URL::action('AdminProductController@getEdit', $product->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="left" title="Edit products"><i class="fa fa-fw fa-edit"></i></a>
                                                        <a href="{{ URL::action('AdminProductController@getOrder', $product->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="bottom" title="View order summary"><i class="fa fa-fw fa-book"></i></a>
                                                        <a href="{{ URL::action('AdminProductController@getCustomer', $product->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="tooltip" data-placement="bottom" title="View customers"><i class="fa fa-fw fa-users"></i></a>
                                                        <a class="btn btn-sm btn-danger btn-flat" data-toggle="modal" data-target="#deleteModal-{{ $product->id }}" data-toggle="tooltip" data-placement="right" title="Delete page"><i class="fa fa-fw fa-trash-o"></i></a>
                                                        <div class="modal fade" id="deleteModal-{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure to delete <strong>{{ $product->name }}</strong> from your database ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {{ Form::open(array('action' => array('AdminProductController@deleteDestroy', $product->id), 'method' => 'delete')) }}
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                        {{ Form::close() }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop()
