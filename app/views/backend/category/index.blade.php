@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    View Users
  </h1>
</section>
<!-- Main content -->
<section class="content">
  @include('backend.layouts.alert')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th width="5%">ID</th>
                <th>Name</th>
                <th>Desc</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($categories as $category)
              <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ ($category->desc == null) ? '-' : $category->desc }}</td>
                <td>
                  <div class="btn-group">
                  <a href="{{ URL::action('AdminCategoryController@getEdit', $category->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit page"><i class="fa fa-fw fa-edit"></i></a>
                  <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal-{{ $category->id }}" data-toggle="tooltip" data-placement="right" title="Delete page"><i class="fa fa-fw fa-trash-o"></i></a>
                  <div class="modal fade" id="deleteModal-{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                        </div>
                        <div class="modal-body">
                          Are you sure to delete <strong>{{ $category->name }}</strong> from your database ?
                        </div>
                        <div class="modal-footer">
                          {{ Form::open(array('action' => array('AdminCategoryController@deleteDestroy', $category->id), 'method'=>'DELETE')) }}
                          <button type="submit" class="btn btn-danger">Delete</button>
                          {{ Form::close() }}
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section><!-- /.content -->
@stop()
