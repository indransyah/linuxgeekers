@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Add Category
  </h1>
</section>
<!-- Main content -->
<section class="content">
  @include('backend.layouts.alert')
  <div class='row'>
    <div class='col-md-6'>
      <div class='box'>
        <!-- /.box-header -->
        <div class='box-body pad'>
          {{ Form::open(array('action' => ['AdminCategoryController@postUpdate', $category->id])) }}
          <div class="form-group">
            <label>Name</label>
            {{ Form::text('name', $category->name, array('class' => 'form-control', 'type' => 'text' )) }}
          </div>
          <div class="form-group">
            <label>Description (Optional)</label>
            {{ Form::textarea('desc', $category->desc, array('class' => 'form-control' )) }}
          </div>
          <div class="box-footer clearfix">
            <button class="pull-right btn btn-info btn-flat" type="submit">Edit <i class="fa fa-edit"></i></button>
          </div>
          {{ Form::close() }}
        </div>
      </div><!-- /.box -->
    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->
<script type="text/javascript">
$(function() {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  CKEDITOR.replace('content');
  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();
});
</script>
@stop()
