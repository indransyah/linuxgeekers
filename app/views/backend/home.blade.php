@extends('backend.layouts.master')
@section('content')
	<section class="content">
		@include('backend.layouts.alert')
		@if (Session::has('success'))
		<!-- <div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Welcome {{ Sentry::getUser()->name }}!</b> {{ Session::get('success') }}
		</div> -->
		@endif
		<div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{ Order::whereType('ready stock')->count(); }}</h3>
                  <p>Orders</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="{{ URL::action('AdminOrderController@getIndex') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{ Order::whereType('pre order')->count(); }}</h3>
                  <p>Pre Orders</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-calendar"></i>
                </div>
                <a href="{{ URL::action('AdminPreOrderController@getIndex') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{ Product::count(); }}</h3>
                  <p>Products</p>
                </div>
                <div class="icon">
                  <i class="ion ion-tshirt-outline"></i>
                </div>
                <a href="{{ URL::action('AdminProductController@getIndex') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>
										<?php
										$group = Sentry::findGroupByName('Customer');
								    $customers = Sentry::findAllUsersInGroup($group);
										echo $customers->count();
										?>
									</h3>
                  <p>Customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-stalker"></i>
                </div>
                <a href="{{ URL::action('AdminCustomerController@getIndex') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
	</section>
@stop
