<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Linux Geekers | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('assets/backend/css/bootstrap.min.css') }}
        <!-- {{ HTML::style('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css') }} -->
        {{ HTML::style('assets/backend/font-awesome-4.2.0/css/font-awesome.min.css') }}
        <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- Ionicons -->
        <!-- {{ HTML::style('//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css') }} -->
        {{ HTML::style('assets/backend/ionicons-2.0.1/css/ionicons.min.css') }}
        <!-- <link href="https://code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- Theme style -->
        {{ HTML::style('assets/backend/css/AdminLTE.css') }}
        <!-- <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" /> -->
        <!-- bootstrap wysihtml5 - text editor -->
        {{ HTML::style('assets/backend/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
        <!-- <link href="../../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- DATA TABLES -->
        {{ HTML::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
        <!-- <link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" /> -->

        {{ HTML::style('assets/backend/jQuery-File-Upload/css/jquery.fileupload.css') }}




        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.0.2 -->
        {{ HTML::script('assets/backend/js/jquery.min.js') }}
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        <!-- Bootstrap -->
        {{ HTML::script('assets/backend/js/bootstrap.min.js') }}
        <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script> -->
        <!-- AdminLTE App -->
        {{ HTML::script('assets/backend/js/AdminLTE/app.js') }}
        <!-- <script src="js/AdminLTE/app.js" type="text/javascript"></script> -->
        <!-- Dropzone -->
        <!-- {{ HTML::script('assets/backend/dropzone/dropzone.js') }} -->
        <!-- {{ HTML::style('assets/backend/dropzone/dropzone.css') }} -->
    </head>
    <body class="skin-black">
        @include('backend.layouts.header')

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="{{ Request::is('adidev') ? 'active' : '' }}">
                            <a href="{{ URL::action('AdminHomeController@getIndex') }}">
                                <i class="fa fa-home"></i> <span>Home</span>
                            </a>
                        </li>
                        <li class="treeview {{ Request::is('adidev/product*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-barcode"></i>
                                <span>Products</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/product/create') ? 'active' : '' }}"><a href="{{ URL::action('AdminProductController@getCreate') }}"><i class="fa fa-plus"></i> Add Product</a></li>
                                <li class="{{ Request::is('adidev/product') ? 'active' : '' }}"><a href="{{ URL::action('AdminProductController@getIndex') }}"><i class="fa fa-eye"></i> View Products</a></li>
                                <!-- <li class="{{ Request::is('adidev/pre-order') ? 'active' : '' }}"><a href="{{ URL::action('AdminPreOrderController@getIndex') }}"><i class="fa fa-flag-o"></i> Pre Order</a></li> -->

                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/category*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-list"></i>
                                <span>Categories</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/category/create') ? 'active' : '' }}"><a href="{{ URL::action('AdminCategoryController@getCreate') }}"><i class="fa fa-plus"></i> Add Category</a></li>
                                <li class="{{ Request::is('adidev/category') ? 'active' : '' }}"><a href="{{ URL::action('AdminCategoryController@getIndex') }}"><i class="fa fa-eye"></i> View Category</a></li>

                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/order*') || Request::is('adidev/pre-order*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Orders</span>
                                @if(Order::unconfirmed()->count() > 0)
                                <span class="label label-danger">{{ Order::unconfirmed()->count() }}</span>
                                @endif
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::action('AdminOrderController@getIndex') }}"><i class="fa fa-eye"></i> Ready Stock</a></li>
                                <li><a href="{{ URL::action('AdminPreOrderController@getIndex') }}"><i class="fa fa-eye"></i> Pre Order</a></li>
                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/payment*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-usd"></i>
                                <span>Payments</span>
                                @if(Payment::unconfirmed()->count() > 0)
                                <span class="label label-danger">{{ Payment::unconfirmed()->count() }}</span>
                                @endif
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/payment/*') ? 'active' : '' }}"><a href="{{ URL::action('AdminPaymentController@getIndex') }}"><i class="fa fa-eye"></i> View Payments</a></li>
                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/shipment*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-truck"></i>
                                <span>Shipments</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/shipment/*') ? 'active' : '' }}"><a href="{{ URL::action('AdminShipmentController@getIndex') }}"><i class="fa fa-eye"></i> View Shipments</a></li>
                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/page*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-file-text"></i>
                                <span>Pages</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/page/create') ? 'active' : '' }}"><a href="{{ URL::action('AdminPageController@create') }}"><i class="fa fa-plus"></i> Add Page</a></li>
                                <li class="{{ Request::is('adidev/page') ? 'active' : '' }}"><a href="{{ URL::action('AdminPageController@index') }}"><i class="fa fa-eye"></i> View Pages</a></li>
                                <!-- <li>{{ HTML::decode(link_to_action('AdminPageController@create', '<i class="fa fa-plus"></i> Add Page')) }}</li> -->
                                <!-- <li>{{ HTML::decode(link_to_action('AdminPageController@index', '<i class="fa fa-eye"></i> View Pages')) }}</li> -->
                                <!-- <li><a href="pages/charts/morris.html"><i class="fa fa-plus"></i> Add Page</a></li> -->
                                <!-- <li><a href="pages/charts/flot.html"><i class="fa fa-eye"></i> View Pages</a></li> -->
                            </ul>
                        </li>

                        <li class="treeview {{ Request::is('adidev/report*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-list-alt"></i>
                                <span>Report</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/report/') ? 'active' : '' }}"><a href="{{ URL::action('AdminReportController@getIndex') }}"><i class="fa fa-list-alt"></i> View Report</a></li>
                            </ul>
                        </li>

                        <!-- <li class="treeview {{ Request::is('adidev/customer*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Customers</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/customer/index') ? 'active' : '' }}"><a href=""><i class="fa fa-eye"></i> View Customers</a></li>
                            </ul>
                        </li> -->
                        <li class="treeview {{ Request::is('adidev/customer*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Customers</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/customer/index') ? 'active' : '' }}"><a href="{{ URL::action('AdminCustomerController@getIndex') }}"><i class="fa fa-eye"></i> View Customers</a></li>
                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/user*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Administrators</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/user/create') ? 'active' : '' }}"><a href="{{ URL::action('AdminUserController@getCreate') }}"><i class="fa fa-plus"></i> Add User</a></li>
                                <li class="{{ Request::is('adidev/user/index') ? 'active' : '' }}"><a href="{{ URL::action('AdminUserController@getIndex') }}"><i class="fa fa-eye"></i> View Users</a></li>
                                <li class="{{ Request::is('adidev/user/profile') ? 'active' : '' }}"><a href="{{ URL::action('AdminUserController@getIndex') }}"><i class="fa fa-user"></i> Profile</a></li>
                            </ul>
                        </li>
                        <li class="treeview {{ Request::is('adidev/setting*') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                <span>Settings</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('adidev/setting/company') ? 'active' : '' }}"><a href="{{ URL::action('AdminSettingController@getCompany') }}"><i class="fa fa-sitemap"></i> Company</a></li>
                                <li class="{{ Request::is('adidev/setting/homepage') ? 'active' : '' }}"><a href="{{ URL::action('AdminSettingController@getHomepage') }}"><i class="fa fa-home"></i> Homepage</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                @yield('content')
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        @include('backend.layouts.footer')

    </body>
</html>
