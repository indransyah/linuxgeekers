        <!-- jQuery 2.0.2 -->
        <!-- {{ HTML::script('assets/backend/js/jquery.min.js') }} -->
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        <!-- Bootstrap -->
        <!-- {{ HTML::script('assets/backend/js/bootstrap.min.js') }} -->
        <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script> -->
        <!-- DATA TABES SCRIPT -->
        {{ HTML::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
        {{ HTML::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
        // <script type="text/javascript">
        //     $(function() {
        //         $("#example1").dataTable();
        //         $('#example2').dataTable({
        //             "bPaginate": true,
        //             "bLengthChange": false,
        //             "bFilter": false,
        //             "bSort": true,
        //             "bInfo": true,
        //             "bAutoWidth": false
        //         });
        //     });
        // </script>
        <script type="text/javascript">
        $(document).ready(function() {
          $('#example1').dataTable({
            //"aaSorting": [[0,'desc']],
            "bSort": false,
            "oLanguage": {
              "sUrl": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Indonesian.json"
            }
          });

          // Modal
          $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.form-action').attr('action', recipient)
            // $('#form-delete').attr('action' + recipient)
          });
        });
    </script>
        <!-- CK Editor -->
        {{ HTML::script('assets/backend/ckeditor/ckeditor.js') }}
        <!-- {{ HTML::script('//cdn.ckeditor.com/4.4.3/standard/ckeditor.js') }} -->
        <!-- Bootstrap WYSIHTML5 -->
        <!-- <script src="../../js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script> -->
        {{ HTML::script('assets/backend/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
        <!-- AdminLTE App -->
        <!-- {{ HTML::script('assets/backend/js/AdminLTE/app.js') }} -->
        <!-- <script src="js/AdminLTE/app.js" type="text/javascript"></script> -->
